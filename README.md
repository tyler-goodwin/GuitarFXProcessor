# README #

### What is this repository for? ###
This is a real time guitar FX processor.

### How do I get set up? ###

ALSA needs to be installed using: 


```
#!
    sudo apt-get install libasound-dev

```



Compile in the src directory with:
```
#!
    make
```

Then to run:
```
#!
    ./fxprocessor
```
### Port Audio and Latency ###
Unix

PortAudio under Unix currently uses a backgroud thread that reads and writes to OSS. This gives you decent but not great latency. But if you raise the priority of the background thread to a very priority then you can get under 10 milliseconds latency. In order to raise your priority you must run the PortAudio program as root! You must also set PA_MIN_LATENCY_MSEC using the appropriate command for your shell.


### Who do I talk to? ###
Tyler Goodwin
tyler.goodwin23@gmail.com