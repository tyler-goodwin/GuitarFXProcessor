/*-------------------------------------

  main.cpp


-------------------------------------*/
#include "main.hpp"

#define SAMPLE_RATE (44100)
#define FRAMES_PER_BUFFER (32)

using namespace std;

typedef struct
{
    float left_phase;
    float right_phase;
}   
paTestData;

void testAquila();

static int fxCallback( const void *inputBuffer, void *outputBuffer,
                           unsigned long framesPerBuffer,
                           const PaStreamCallbackTimeInfo* timeInfo,
                           PaStreamCallbackFlags statusFlags,
                           void *userData );

int main(){
  PaError err = Pa_Initialize(); 

  if( err != paNoError ){
    cout << "Error:" << Pa_GetErrorText(err) << endl;
    return 0;
  }
  cout << endl;
  cout << endl;
  cout << "Guitar FX Processor"<< endl;
  cout << "==========================" << endl;

  cout << Pa_GetVersionInfo()->versionText << endl;

  cout << "Aquila " << Aquila::VERSION_STRING << endl << endl;
  
  //testAquila();

  //create FX processors
  FX fx;
  // Intensity, feedback, volume, duration, mix
  fx.delay = new Delay(0.7, 0.8, 1.0, 0.2, 0.0);
  //Delay * delay = new Delay();
  //Gain, tone, volume
  fx.distortion = new Distortion(500.0, 1, 0.5);


  PaStream *stream;
  /* Open an audio I/O stream. */
  err = Pa_OpenDefaultStream( &stream,
                              1, // Input channels
                              1, //output channels         
                              paFloat32,  
                              SAMPLE_RATE,
                              FRAMES_PER_BUFFER,        
                              fxCallback, 
                              &fx );
  
  PaAlsa_EnableRealtimeScheduling(stream, 1);

  err = Pa_StartStream( stream );
  // cout << "Press Enter to End Tone";
  // cin.ignore();
  cout << "Enter tone value, -1 to quit." << endl;
  float in = 1.0;
  while(in != -1.0){
    cin >> in;
    fx.distortion->setTone(in);
  }
  err = Pa_StopStream( stream );

  err = Pa_Terminate();

  //Cleanup
  delete fx.delay;
  delete fx.distortion;

  if(err != paNoError)
    cout << "Error: " << Pa_GetErrorText(err) << endl;
  return 0;

}

void testAquila(){
  cout << "Testing Aquila" << endl;
  cout << "==============================" << endl;
  Aquila::TriangleGenerator generator(1000);
  generator.setFrequency(25).setAmplitude(255).generate(64);
  Aquila::TextPlot plot("Sawtooth wave");

  generator.setWidth(0.5).generate(64);
  
  generator.setWidth(0.18).generate(64);
  plot.setTitle("Triangle wave, slope width = 18%");
  plot.plot(generator);
}

static int fxCallback( const void *inputBuffer, void *outputBuffer,
                           unsigned long framesPerBuffer,
                           const PaStreamCallbackTimeInfo* timeInfo,
                           PaStreamCallbackFlags statusFlags,
                           void *userData )
{
    // Hide compilers unused warnings
    (void) timeInfo;
    (void) statusFlags;

    FX* fx = (FX*) userData;
    Delay *delay = fx->delay;
    Distortion *distortion = fx->distortion;

    float *out = (float*)outputBuffer;
    float *in = (float*) inputBuffer;
    //buffer to route between distortion and delay
    float temp[FRAMES_PER_BUFFER] = {};

    distortion->process(in, temp, framesPerBuffer);
    delay->process(temp, out, framesPerBuffer);
    
    return 0;
}