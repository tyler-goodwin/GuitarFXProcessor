#ifndef DELAY
#define DELAY

#include <array>
const int maxDelay = 5000;

class Delay {

private:
  std::array<std::array<float, 32>, maxDelay> previousBuffers;
  int write_pos;
  
  float intensity, feedback, volume, duration, mix;

  void initPreviousBuffers();
  int getReadPos();  

public:
  Delay();
  Delay(float int_in, float fb_in, float vol_in, float duration, float mix_in);
  ~Delay();

  void process(const float* inputBuffer, float* outputBuffer, unsigned long frameSize);

  float getIntensity();
  void setIntensity(float);
  
  float getFeedback();
  void setFeedback(float);

  float getVolume();
  void setVolume(float);

  float getDuration();
  void setDuration(float);


};

#endif //DELAY