#ifndef DISTORTION
#define DISTORTION

#include <array>
#include <cmath>
#define SAMPLE_RATE (44100)
#define MAX_FREQ (5000)

class Distortion {
private:
  float gain, tone, volume, dt, rc, alpha;

public:
  
  Distortion();
  Distortion(float gain, float tone, float volume);
  ~Distortion();

  void process(const float* inputBuffer, float* outputBuffer, unsigned long frameSize);

  float getGain();
  void setGain(float in);

  float getTone();
  void setTone(float in);

  float getVolume();
  void setVolume(float in);

};

#endif //DISTORTION