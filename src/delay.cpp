#include "delay.hpp"

Delay::Delay(){
  //defaults
  intensity = 0.7;
  feedback = 0.3;
  volume = 0.8;
  duration = 0.2;
  mix = 0.5;

  initPreviousBuffers();
}

Delay::Delay( float int_in,
              float fb_in, 
              float vol_in, 
              float duration,
              float mix_in){
  
  intensity = int_in;
  feedback = fb_in;
  volume = vol_in;
  this->duration = duration;
  mix = mix_in;

  initPreviousBuffers();
}

Delay::~Delay(){
}

void Delay::initPreviousBuffers(){
  write_pos = 0;
  for(int i = 0; i < maxDelay;  i++){
    previousBuffers[i].fill(0.0);
  }

}

int Delay::getReadPos(){
  int read = write_pos - maxDelay*duration;
  if(read < 0){
    read = maxDelay + read;
  }
  return read;
}

void Delay::process(const float* inputBuffer, float* outputBuffer, unsigned long frameSize){
  float prev = 0;

  if(write_pos == maxDelay){
    write_pos = 0;
  }

  for(unsigned long i = 0; i < frameSize; i++){
    prev = previousBuffers[getReadPos()][i]*intensity;
    //write input + feedback to memory
    previousBuffers[write_pos][i] = inputBuffer[i] + (prev)*feedback;
    //send read position to output 
    outputBuffer[i] = ((inputBuffer[i] * (1.0-mix)) + (prev*mix))*volume;
  }
  //Increment position
  write_pos++;
}

float Delay::getIntensity(){
  return intensity;
}

void Delay::setIntensity(float in){
  intensity = in;
}

float Delay::getFeedback(){
  return feedback;
}

void Delay::setFeedback(float in){
  feedback = in;
}

float Delay::getVolume(){
  return volume;
}

void Delay::setVolume(float in){
  volume = in;
}

float Delay::getDuration(){
  return duration;
}

void Delay::setDuration(float in){
  duration = in;
}
