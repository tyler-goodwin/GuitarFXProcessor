#include "distortion.hpp"


Distortion::Distortion(){
  gain = 1.5;
  tone = 1.0;
  volume = 0.7;
}

Distortion::Distortion(float gain,
                       float tone,
                       float volume){
  this->gain = gain;
  this->tone = tone;
  this->volume = volume;
  //Inital calulation for low pass filter
  dt = 1.0/SAMPLE_RATE;
  rc = 1.0/(pow(10,tone-1)*MAX_FREQ*2*3.14);
  alpha = dt/(rc+dt); 
}

Distortion::~Distortion(){}

void Distortion::process(const float* inputBuffer, float* outputBuffer, unsigned long frameSize){
  float temp = 0;
  static float prev_samp = 0;

  for(unsigned long i = 0; i < frameSize; i++){
    //Distort signal
    temp = inputBuffer[i] * gain;
    if(temp > 1.0){
      temp = 1.0;
    }
    else if(temp < -1.0){
      temp = -1.0;
    }
    //filter then and set volume
    outputBuffer[i] = (prev_samp + alpha*(temp - prev_samp))*volume;
    prev_samp = outputBuffer[i];  
  }
}

float Distortion::getGain(){
  return gain;
}

void Distortion::setGain(float in){
  gain = in;
}

float Distortion::getTone(){
  return tone;
}

void Distortion::setTone(float in){
  tone = in;
  rc = 1.0/(tone*MAX_FREQ*2*3.14);
  alpha = dt/(rc+dt);
}

float Distortion::getVolume(){
  return volume;
}

void Distortion::setVolume(float in){
  volume = in;
}





