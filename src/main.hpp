/*-------------------------------------

  main.hpp

  Main entry point for guitar fx processor
  Handles threading and passing parameters to fx

  Author: Tyler Goodwin
  Last Modified: 28-4-2017

-------------------------------------*/
#include "lib/portaudio/portaudio.h"
#include "lib/portaudio/pa_linux_alsa.h"
#include "lib/aquila/aquila.h"

#include <iostream>

// Include fx
#include "delay.hpp"
#include "distortion.hpp"
#include "pitch_shift.hpp"

typedef struct FX {
  Delay* delay;
  Distortion* distortion;
} FX;

